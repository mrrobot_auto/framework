package com.APIControls;

import com.APIControls.DTOs.AccessTokenDto;
import com.ControlImplementation.RequestBuilder;
import com.ControlImplementation.RestCall;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.net.URI;
import java.net.URISyntaxException;

public class TokenActions {
    private URI uri;

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public AccessTokenDto generateToken() throws URISyntaxException {
        try {
            this.setUri(new URI("https://demoqa.com/Account/v1/GenerateToken"));
            RequestBuilder req = new RequestBuilder()
                    .setUri(getUri())
                    .setEntity("{\n" +
                            "  \"userName\": \"sujay\",\n" +
                            "  \"password\": \"Qwerty!@#45\"\n" +
                            "}")
                    .build();
            return new Gson().fromJson(new RestCall().post(req), AccessTokenDto.class);
        } catch (URISyntaxException | JsonSyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

}
