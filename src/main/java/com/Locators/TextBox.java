package com.Locators;

public enum TextBox {
    USER_NAME("#userName"),
    PASSWORD("#password");
    private String value;
    TextBox(String value) {
        this.value = value;
    }
    public String getValue(){
        return value;
    }
}
