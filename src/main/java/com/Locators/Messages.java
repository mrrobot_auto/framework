package com.Locators;

public enum Messages {
    NAME("#name");
    private String value;
    Messages(String value) {
        this.value = value;
    }
    public String getValue(){
        return this.value;
    }
}
