package com.UIControls;

import com.ControlImplementation.CoreControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BrowserControl {

    public void navigate(String url) {
        try {
            WebDriver driver = CoreControl.getDriver();
            if (isUrl(url)) {
                driver.navigate().to(url);
                //new UrlChecker().waitUntilAvailable(30L,TimeUnit.SECONDS);
            } else {
                System.out.println("URL is not valid");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isUrl(String url) {
        return true;
    }

    public void start() {
        try {
            System.setProperty("webdriver.chrome.driver", "D:\\POMFW\\src\\main\\resources\\drivers\\chromedriver.exe");
            WebDriver driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(10L, TimeUnit.MINUTES);
            driver.manage().timeouts().pageLoadTimeout(10L, TimeUnit.MINUTES);
            driver.manage().timeouts().setScriptTimeout(10L, TimeUnit.MINUTES);

            driver.manage().window().maximize();
            CoreControl.getInstance().setDriver(driver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void tearDown() {
        WebDriver driver = CoreControl.getDriver();
        if (driver != null) {
            driver.quit();
        }
    }
}
