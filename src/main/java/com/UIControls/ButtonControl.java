package com.UIControls;

import com.ControlImplementation.CoreControl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ButtonControl {

    private WebElement findControl(String locator) {
        try {
            return CoreControl.getDriver().findElement(By.cssSelector(locator));
        } catch (Exception staleElementReferenceException) {
            staleElementReferenceException.printStackTrace();
        }
        return null;
    }

    public void click(String buttonNameLocator) {
        try {
            WebElement but = findControl(buttonNameLocator);
            if (but != null && but.isDisplayed() && but.isEnabled()) {
                but.click();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
