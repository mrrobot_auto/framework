package com.UIControls;

import com.ControlImplementation.CoreControl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class MessageControl {
    private WebElement findControl(String locator) {
        try {
            return CoreControl.getDriver().findElement(By.cssSelector(locator));
        } catch (Exception staleElementReferenceException) {
            staleElementReferenceException.printStackTrace();
        }
        return null;
    }
    public String getMessage(String locator) {
        String res = null;
        try {
            WebElement textbox = findControl(locator);
            if (textbox != null) {
                res = textbox.getText();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
