package com.ControlImplementation;

import org.openqa.selenium.WebDriver;

public class CoreControl {
    private WebDriver driver;
    private static CoreControl instance;

    public static void setInstance(CoreControl instance) {
        CoreControl.instance = instance;
    }

    private CoreControl() {
        this.setDriver(driver);
    }

    public static WebDriver getDriver() {
        if (instance == null) {
            synchronized (CoreControl.class) {
                if(instance == null) {
                    setInstance(new CoreControl());
                }
            }
        }
        return instance.driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public static CoreControl getInstance() {
        if (instance == null) {
            synchronized (CoreControl.class) {
                if(instance == null) {
                    setInstance(new CoreControl());
                }
            }
        }
        return instance;
    }

}
