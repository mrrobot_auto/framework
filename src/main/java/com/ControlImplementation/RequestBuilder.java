package com.ControlImplementation;

import java.net.URI;

public class RequestBuilder {
    private URI uri;
    private String entity;
    private String token;

    public RequestBuilder() {

    }

    public RequestBuilder(URI uri, String entity, String token) {
        this.uri = uri;
        this.entity = entity;
        this.token = token;
    }

    public URI getUri() {
        return uri;
    }

    public RequestBuilder setUri(URI uri) {
        this.uri = uri;
        return this;
    }

    public String getEntity() {
        return entity;
    }

    public RequestBuilder setEntity(String entity) {
        this.entity = entity;
        return this;
    }

    public String getToken() {
        return token;
    }

    public RequestBuilder setToken(String token) {
        this.token = token;
        return this;
    }

    public RequestBuilder build() {
        return new RequestBuilder(this.uri, this.entity, this.token);
    }
}
