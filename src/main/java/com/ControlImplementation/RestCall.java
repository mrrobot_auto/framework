package com.ControlImplementation;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class RestCall {
    public String post(RequestBuilder builder) {
        try {
            CloseableHttpResponse response;
            HttpPost request = new HttpPost(builder.getUri());
            request.setHeader("Authorization", builder.getToken());
            request.setEntity(new StringEntity(builder.getEntity(), ContentType.APPLICATION_JSON));
            CloseableHttpClient httpClientBuilder = HttpClients.custom().build();
            response = httpClientBuilder.execute(request);
            return EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
