package com.Pages;

import com.Locators.Buttons;
import com.Locators.Messages;
import com.Locators.TextBox;
import com.UIControls.ButtonControl;
import com.UIControls.MessageControl;
import com.UIControls.TextBoxControl;

public class LoginPage {
    ButtonControl buttonControl = new ButtonControl();
    TextBoxControl textBoxControl = new TextBoxControl();
    MessageControl messageControl = new MessageControl();

    public void clickOnLogin() {
        buttonControl.click(Buttons.LOGIN.getValue());
    }

    public void enterUserName(String username) {
        textBoxControl.setText(TextBox.USER_NAME.getValue(), username);
    }

    public void enterPassword(String password) {
        textBoxControl.setText(TextBox.PASSWORD.getValue(), password);
    }

    public String getErrorMessage() {
        return messageControl.getMessage(Messages.NAME.getValue());
    }
}
