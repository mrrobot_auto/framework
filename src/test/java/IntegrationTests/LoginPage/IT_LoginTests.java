package IntegrationTests.LoginPage;

import com.Pages.LoginPage;
import com.UIControls.BrowserControl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class IT_LoginTests {

    @BeforeEach
    public void startUp() {
        BrowserControl browserControl = new BrowserControl();
        browserControl.start();
        browserControl.navigate("https://demoqa.com/login");
    }

    LoginPage loginPage = new LoginPage();
    @Test
    public void validLogin() {
        try {
            loginPage.enterUserName("sujay");
            loginPage.enterPassword("Qwerty!@#45");
            loginPage.clickOnLogin();
            Thread.sleep(5000L);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void invalidLogin(){
        try {
            loginPage.enterUserName("sujay");
            loginPage.enterPassword("Qwerty12345");
            loginPage.clickOnLogin();
            Assertions.assertEquals("Invalid username or password!",loginPage.getErrorMessage());
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @AfterEach
    public void tearDown() {
        BrowserControl browserControl = new BrowserControl();
        browserControl.tearDown();
    }
}
